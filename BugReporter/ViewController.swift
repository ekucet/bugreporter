//
//  ViewController.swift
//  BugReporter
//
//  Created by Erkam on 23/07/15.
//  Copyright (c) 2015 Erkam KÜCET. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        let bugReport = BugReport()
        bugReport.createReportFileInDocumentDirectory("TestFile.txt")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendMail(sender: AnyObject) {
        
        let bugReport = BugReport()
        let status = bugReport.createReportFileInDocumentDirectory("TestFile.txt")
        
        if status == true {
        
            let mailController = MFMailComposeViewController()
            let recipies = ["ekucet@gmail.com"]
            let data = bugReport.getReportFileData("TestFile.txt")
            
            mailController.mailComposeDelegate = self
            mailController.addAttachmentData(data, mimeType: "text/plain", fileName: "TestFile.txt")
            mailController.setToRecipients(recipies)
            mailController.setSubject("Thank You Erkam")
            
            self.presentViewController(mailController, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        dismissViewControllerAnimated(true, completion: nil)
        if error == nil {
            
            let alertController = UIAlertController(title: "Bug Reporter", message: "Success", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(okAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }else {
            
            let alertController = UIAlertController(title: "Bug Reporter", message: "Error: \(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(okAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }

}

